from django.apps import AppConfig


class UrsulaClaimsV1Config(AppConfig):
    name = 'ursula_claims_v1'
