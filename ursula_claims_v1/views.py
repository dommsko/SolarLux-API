from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from .models import Claim
from .serializers import ClaimSerializer
from .permissions import IsOwner

from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


"""
class ClaimInput(generics.CreateAPIView):
    serializer_class = ClaimSerializer
"""

class ClaimInput(APIView):
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        #print(request.user.IsAuthenticated)
        serializer = ClaimSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=self.request.user)
            full = serializer.data
            #print(full)
            return Response(full, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ClaimStatus(APIView):
    permission_classes = (IsOwner, )
    def get_object(self, pk):
        try:
            return Claim.objects.get(pk=pk)
        except Claim.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        claim = self.get_object(pk)
        if str(request.user.username) == str(claim.owner):
            serializer = ClaimSerializer(claim)
            return Response(serializer.data)
        else:
        	return Response(status=status.HTTP_401_UNAUTHORIZED)

##########

class ClaimInput2(APIView):
    queryset = Claim.objects.all()
    serializer_class = ClaimSerializer
    renderer_classes = (JSONRenderer, )

    @api_view(['POST'])
    @permission_classes((IsAuthenticated, ))
    def post(self, request, format=None):
        #print(request.data)
        serializer = ClaimSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=self.request.user)
            full = serializer.data
            print(full)
            return Response(full, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @api_view(['GET'])
    @permission_classes((IsOwner, ))
    def get_object(self, pk):
        try:
            return Claim.objects.get(pk=pk)
        except Claim.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        claim = self.get_object(pk)
        serializer = ClaimSerializer(claim)
        return Response(serializer.data)
