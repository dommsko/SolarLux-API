from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^claim/$', views.ClaimInput.as_view()),
    url(r'^claim/(?P<pk>[0-9a-f-]+)/$', views.ClaimStatus.as_view()),
    #path('status/<uuid:id>', views.ClaimStatus.as_view())
]