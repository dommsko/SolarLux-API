from __future__ import unicode_literals
from django.db import models
import uuid
from django.contrib.auth.models import User

#class WalletField(models.CharField):

#class CountryField(models.CharField):

class Claim(models.Model):
	firstName = models.CharField(max_length=255)
	lastName = models.CharField(max_length=255)
	email = models.EmailField()
	walletAddress = models.CharField(max_length=34)
	address = models.CharField(max_length=255)
	city = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	zipCode = models.CharField(max_length=10)
	country = models.CharField(max_length=255)
	nameplate = models.DecimalField(max_digits=10, decimal_places=4)
	installDate = models.DateField()
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	owner = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
	#statusCode = models.CharField(max_length=255)