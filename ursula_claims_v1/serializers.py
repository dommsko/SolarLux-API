from .models import Claim
from rest_framework import serializers


class ClaimSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Claim
        fields = ('firstName', 'lastName', 'email', 'walletAddress', 'address', 'city', 'state', 'zipCode', 'country', 'nameplate', 'installDate', 'id', 'owner')