from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Claim
from .serializers import ClaimSerializer


class ClaimInput(APIView):
    def get(self, request, format=None):
        claims = Claim.objects.all()
        serializer = ClaimSerializer(claims, many=True)
        return Response(serializer.data)