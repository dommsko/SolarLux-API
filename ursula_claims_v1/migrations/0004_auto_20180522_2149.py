# Generated by Django 2.0.5 on 2018-05-22 21:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ursula_claims_v1', '0003_claim_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claim',
            name='uuid',
            field=models.UUIDField(default='de305d54-75b4-431b-adb2-eb6b9e546013'),
        ),
    ]
