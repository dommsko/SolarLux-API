# Generated by Django 2.0.5 on 2018-05-22 21:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ursula_claims_v1', '0004_auto_20180522_2149'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='claim',
            name='uuid',
        ),
    ]
